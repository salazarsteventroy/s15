// operators
/*
 add + - sum
 subtract - = difference
 multiplay * = product
 divide / = quotient
 modulus % = remainder
*/

function mod(remainder) {
	return 9 % 2;
}

console.log(mod());

// assignment operator (=)
	
/*
	+=	(addition)
	-=	(subtraction)
	*=	(multiplication)
	/= (division)
	%= (modulo)

*/

	let x = 1;

	let sum =1;
	// sum = sum + 1;

	sum += 1;

	console.log(sum);

// Increment and Decrement (++, --)
/*operators that add or subtract values by 1 and reassigns the 
value of the variable where the incremenet/decrement was applied to*/

let z = 1;
// pre incremenet
let increment = ++z;
console.log("result of pre-increment" + increment);
console.log("result of pre-increment" + z);

// post increment

increment = z++;
console.log("result of post-increment: " + increment);
console.log('result of post ncremenet: ' + z);

// pre decrement
let decrement =--z;
console.log("result of pre-dedcrement : " + decrement);
console.log("result of pre-decremenet" + z);

// post decrement
decrement =z--;
console.log("result of post-decrement: " + decrement);
console.log("result of post-decrement: " + z);


// comparison operators

// equality operator (==)

let juan = 'juan';

console.log(1==1);
console.log(0 == false);
console.log('juan' == juan);
// strict equality (====)
console.log(1===true);

// inequality operator (!=)
console.log("inequality operator");
console.log(1 != 1);
console.log ('Juan' != juan);

// strict inequality
console.log(0 !== false);

// other comparison operators

/*
	> - greater than
	< - less than
	>= greater than or equal
	<= less than or equal
*/

let isLegalAge = true;
let isRegistered = false;
// logical operators
/*
	and operator (&&) - returns true if all operands are true
	true && true = true
	false && true = false
	true && false = false
	false && false = false	

	or operator (||) - returns true if at least on operands is true
	true && true = true
	true && false = true
	false && true = true
	false && false = false
*/

// and operator
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND operator: " + allRequirementsMet);

// or operator
let someRequirementsMet = isLegalAge || isRegistered;
console.log("results of logical OR operator :"  + someRequirementsMet);


// Selection Control Structures

/* IF statement
-execute a statement if a specified condition is true
	Syntax
	if(condition){
	statement/s;
	}
*/

let num = -1;

if (num < 0) {
	console.log('hello');
}

let num1 = 50;

if (num1 >= 10){
	console.log('welcome to zuitt');
	}

/*If-else statement 
	-executes a statement fi the previous condition
	returns false.
	Syntax:
	if(condition){
		statement/s;
	}
	else{
		statement/s;
	}

*/

num2 = 5

if(num2 >= 10){
	console.log("Number is greater or equal to 10");
}
else{
	console.log("Number is not greater or equal to 10");
}

/*let age = parseInt(prompt("please provide age:"));

if (age > 59){
	console.log ("senior age");
}
else{
	alert("invalid age");
}*/

// IF-ELSEIF-ELSE statement
/*
	syntax:
	if(condition){
		statement/s;
	}
	else if(condition){
		statement/s;
	}
	else if(condition){
		statement/s;
	}
	.
	.
	.
	.
	else{
		statement/s;
	}


	1- quezon city
	2- valenzuela city
	3- pasig city
	4- taguig
*/
/*
let city = parseInt(prompt("Enter a number:"));

if(city === 1){
	alert("Welcome to Quezon City");
}
else if (city === 2){
	alert("wecome to valenzuela city");
}
else if(city === 3){
	alert("welcome to pasig city");
}
else if (city === 4){
	alert("welcome to taguig city");
}
else {
	alert("invalid number");
}

*/

let message = '';

function determineTyphoonIntensity(windSpeed) {
	if(windSpeed < 30){
		return 'not a typhoon yet';
	}
	else if(windSpeed <= 61){
		return 'tropical depression detected.';
	}
	else if(windSpeed >= 61 && windSpeed <= 88){
		return 'tropical storm detected';
	}
	else if (windSpeed >= 89 && windSpeed <= 117){
		return 'typhoon detected' ;
	}
	else{
		return 'typhoon detected';
	}
	
}

message = determineTyphoonIntensity(70);
console.log(message);


// ternary operator
/*
syntax:
	(condition) ? ifTrue :ifFalse
*/

/*et ternaryResult = (1 < 18) ? true : false;
console.log("result of ternary operator : " + ternaryResult);*/

/*let name;

function isOfLegalAge(){
	name = 'john';
	return 'you are of the legal age limit';
}
 function isUnderAge() {
 	name = 'jane';
 	return 'You are under the age limit';
 }

 let age = parseInt(prompt("what iis your age"));
 let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge()
 alert("result of Ternary Operator in functions: " + legalAge + ',' + name);*/

 // Switch statement
 /* Syntax: 
		switch (expression){
			case value:
				statement/s;
				break;
			case value2;
				statement/s;

			case valueN;
				statement/s;
		}
 */

 let day = prompt ("what day of the week is it today?").toLowerCase();

 switch (day){
 		case 'sunday':
 			alert (" the color of the dy is red");
 			break;
 		case 'monday':
 			alert ("the color of the day is orange");
 			break;
 		case 'tuesday':
 			alert ("the color of the day is yellow");
 			break;
 		case 'wednesday':
 			alert ("the color of the day is blue");
 			break;
 		case 'thursday':
 			alert ("the color of the day is green");
 			break;
 		case 'friday':
 			alert ( "the color of the day is blue");
 		case 'saturday':
 			alert ( "the color of the day is indigo");
 			break;
 		default:
 			alert("please input a valid day");
 			
 // Try-Catch-Finally Statement - commonly used for error handling

 function showIntensityAlert(windSpeed){
 	try{
 		alerat(determineTyphoonIntensity(windSpeed));
 	}
 	catch(error){
 		console.log(typeof error);
 		console.warn(error.message);
 	}
 	finally{
 		alert('Intensity updates will show new alert')
 	}
}
 showIntensityAlert(56);